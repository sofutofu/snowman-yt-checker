# Date is useful!
$today = Get-Date -Format "yyyyMMdd"

# Load the current files
$existing_jr_channel = Get-Content -Path "playlists/latest_jr.txt" | ConvertFrom-Json
$existing_snowman_channel = Get-Content -Path "playlists/latest_snowman.txt" | ConvertFrom-Json

# Get the new playlist files from the Jr channel and from the Snow Man channel
$jr_channel = youtube-dl -j --flat-playlist "https://www.youtube.com/playlist?list=PLBw8EmMNM8vx5NmhisNsAPJk76QABiOBk" | Tee-Object -file "playlists/jr_$today.txt" | Tee-Object -file "playlists/latest_jr.txt" | ConvertFrom-Json
$snowman_channel = youtube-dl -j --flat-playlist "https://www.youtube.com/c/J_SNOWMAN/videos" | Tee-Object -file "playlists/snowman_$today.txt" | Tee-Object -file "playlists/latest_snowman.txt" | ConvertFrom-Json

# Get the existing changelog
$changelog = Get-Content "changelog.md" -Raw

# Add the updates
$adds = $today + "`n===========================`n`n"

function New-Section {
    param (
        $Which,
        $PlaylistOutput, # Newly downloaded
        $ExistingPlaylist # Old version
    )

    $section = "## "+$Which+"`n"

    if ($PlaylistOutput | Where-Object {$_.url -ne $_.id}) {
        Write-Output "URLs no longer match IDs, check the file."
        break; 
    } else {
        # Check what's new and what's removed, and write those lines
        $new = $PlaylistOutput | Where-Object {$_.id -notin $ExistingPlaylist.id} # New
        $removed = $ExistingPlaylist | Where-Object {$_.id -notin $PlaylistOutput.id } # Existing

        if($new | Measure-Object | ForEach-Object {$_.Count}) {
            foreach($video in $new) {
                $section = $section + ("**$([char]0x2606) NEW** " + $video.title + " (" + $video.id + ")`n")
             }
             
             $section = $section + "`n"
        }
    
        if($removed | Measure-Object | ForEach-Object {$_.Count}) {
            foreach($video in $removed) {
                $section = $section + ("**$([char]0x2620) REMOVED** " + $video.title + " (" + $video.id + ")`n")
            }
    
            $section = $section + "`n"
        }

        # Flag potential downloads and ask for OK to do so
        Write-Output "About to download these files:"
        $new | ForEach-Object {Write-Output ($_.title +"`n")}
        $download = Read-Host "Okay to download? (0/1)"
        if ([int]$download) {
            $new | ForEach-Object {$_.id | youtube-dl}
        }

        return($section);
    }

}

# Add sections
$adds = $adds + (New-Section -Which "Jr Channel" -PlaylistOutput $jr_channel -ExistingPlaylist $existing_jr_channel )
$adds = $adds + (New-Section -Which "Snow Man Channel" -PlaylistOutput $snowman_channel -ExistingPlaylist $existing_snowman_channel )

# Write out to overwrite changelog
($adds + $changelog) | Out-File "changelog.md"